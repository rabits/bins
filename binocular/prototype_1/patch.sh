
root_dir=/build
PATCH_DIR="${root_dir}/patches/"
BUILDROOT=${PWD}

echo "Applying patches ..."
if [ ! -e $PATCH_DIR ]
then
        echo -e "$RED $PATCH_DIR : Not Found $ENDCOLOR"
        exit 1
fi

cd $PATCH_DIR
patch_root_dir="$PATCH_DIR"
android_patch_list=$(find . -type f -name "*.patch" | sort) &&
for android_patch in $android_patch_list; do
        android_project=$(dirname $android_patch)
        echo -e "$YELLOW   applying patches on $android_project ... $ENDCOLOR"
        cd $BUILDROOT/$android_project
        if [ $? -ne 0 ]; then
                echo -e "$RED $android_project does not exist in BUILDROOT:$BUILDROOT $ENDCOLOR"
                exit 1
        fi
        git am --3way $patch_root_dir/$android_patch
	[ $? -eq 0 ] || git am --abort
done
